const MongoClient = require("mongodb").MongoClient,
    url = "mongodb://127.0.0.1:27017";

let getYears = function (dbName) {
    return new Promise(function (resolve, request) {
        let years = {};
        MongoClient.connect(url, {
            useNewUrlParser: true
        }, function (err, conn) {
            if (err) {
                console.log("Database not found. " + err);
                resolve(err);
            } else {
                console.log("connected database");
            }
            let dbObject = conn.db(dbName);
            let collection = dbObject.collection("matches");
            collection.aggregate([{
                "$group": {
                    _id: "$season"
                }
            }, {
                "$sort": {
                    _id: 1
                }
            }]).toArray(function (err, res) {
                years = res;
                resolve(years);
            })

        })
    })
}


let getTeams = function (dbName, year) {
    return new Promise(function (resolve, request) {
        let teams = {};
        MongoClient.connect(url, {
            useNewUrlParser: true
        }, function (err, conn) {
            if (err) {
                console.log("Database not found. " + err);
                resolve(err);
            } else {
                console.log("connected database");
            }
            let dbObject = conn.db(dbName);
            let collection = dbObject.collection("matches");
            collection.aggregate([{
                $project: {
                    id: 1,
                    season: 1,
                    team1: 1
                }
            }, {
                $match: {
                    season: parseInt(year)
                }
            }, {
                $sort: {
                    id: 1
                }
            }, {
                $group: {
                    _id: "$team1"
                }
            }]).toArray(function (err, res) {
                // console.log(res);
                teams = res;
                resolve(teams);
            })

        })
    })
}

let getPlayers = function (dbName, team) {
    console.log(team);
    return new Promise(function (resolve, request) {
        let players = {};
        MongoClient.connect(url, {
            useNewUrlParser: true
        }, function (err, conn) {
            if (err) {
                console.log("Database not found. " + err);
                resolve(err);
            } else {
                console.log("connected database");
            }
            let dbObject = conn.db(dbName);
            let collection = dbObject.collection("deliveries");
            collection.aggregate([{
                $project: {
                    batting_team: 1,
                    batsman: 1
                }
            }, {
                $match: {
                    batting_team: team
                }
            }, {
                $group: {
                    _id: "$batsman"
                }
            }]).toArray(function (err, res) {
                // console.log(res);
                players = res;
                resolve(players);
            })

        })
    })
}
module.exports = {
    getYears,
    getTeams,
    getPlayers
}