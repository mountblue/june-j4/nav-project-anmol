const express = require("express"),
    routes = require('./routes/api'),
    path = require("path"),
    app = express();

app.use(express.static("public/js"));
app.use('/api', routes);

app.get('/', function (req, res) {
    res.sendFile(path.resolve("views/index.html"));
})

app.listen(process.env.port || '5000', 'localhost', function () {
    console.log("Started the server");
});