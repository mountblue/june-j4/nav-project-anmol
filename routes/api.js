const express = require("express"),
    router = express.Router(),
    path = require("path"),
    filePath = path.resolve("api-functions.js"),
    fileName = require(filePath),
    dbName = "datadb";

//get years data from the database
router.get('/seasons', function (req, res) {
    fileName.getYears(dbName).then(function(data){
        res.send(JSON.stringify(data));
    })
})

//get season data from the database
router.get('/seasons/:season', function (req, res) {
    // console.log(req.params.season);
    fileName.getTeams(dbName,req.params.season).then(function(data){
        res.send(JSON.stringify(data));
    })
})
//get data for teams in particular season from the database
router.get('/seasons/:season/teams', function (req, res) {
    // console.log(req.params.season);
    fileName.getTeams(dbName,req.params.season).then(function(data){
        res.send(JSON.stringify(data));
    })
})
//get team data from the database
router.get('/seasons/:season/teams/:team', function (req, res) {
    // console.log(req.params.team);
    fileName.getPlayers(dbName,req.params.team).then(function(data){
        res.send(JSON.stringify(data));
    })
})
//get team data from the database
router.get('/seasons/:season/teams/:team/players', function (req, res) {
    // console.log(req.params.team);
    fileName.getPlayers(dbName,req.params.team).then(function(data){
        res.send(JSON.stringify(data));
    })
})
module.exports = router;