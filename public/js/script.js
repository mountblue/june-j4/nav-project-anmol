$.ajax({
    method: 'GET',
    url: "http://localhost:5000/api/seasons",
    dataType: 'json'
}).done(function (data) {
    // console.log(data);
    $(".years-div").append(`<ul class="seasons-ul"></ul>`);

    $.map(data, function (data, i) {
        $(".seasons-ul").append(`<li class="season-li"> ${data._id} </li>`)
    });
});

$(".years-div").on('click', function (event) {
    // console.log(event.target.className);
    if (event.target.lastChild.lastChild === null && event.target.className === "season-li") {
        $.ajax({
            method: 'GET',
            url: "http://localhost:5000/api/seasons/" + event.target.outerText,
            dataType: 'json'
        }).done(function (data) {
            $(event.target).append(`<div class="teams-div"><ul class="teams-ul"></ul></div>`);
            $.map(data, function (data, i) {
                $(event.target.lastChild.lastChild).append(`<li class="team-li"> ${data._id} </li>`)
            });
        });
    } else {
        $(event.target.lastChild.lastChild).remove();
    }

    if (event.target.lastChild.lastChild === null && event.target.className === "team-li") {
        // console.log("hello jiiiiiiiiii");
        // console.log(event.target.outerText);
        $.ajax({
            method: 'GET',
            url: "http://localhost:5000/api/seasons/" + parseInt(event.target.parentElement.parentElement.parentElement.childNodes[0].data) + "/teams/" + event.target.outerText,
            dataType: 'json'
        }).done(function (data) {
            $(event.target).append(`<div class="players-div"><ul class="players-ul"></ul></div>`);
            $.map(data, function (data, i) {
                $(event.target.lastChild.lastChild).append(`<li class="player-li"> ${data._id} </li>`)
            });
        });
    } else {
        console.log("hello else")
        if(event.target.lastChild.lastChild !== null){
            $(event.target.lastChild.lastChild).remove();
        }
        
    }

})